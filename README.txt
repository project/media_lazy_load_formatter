# Media Lazy Load Formatter

## Description
This module allows the option of lazy loading different forms of media
via field formatters. Currently only supports the image field formatter.

It uses the JQuery Lazy Load Xt library to lazy load the images on scroll.

## Installation:

Enable module.

## Usage:
Visit any entity's display mode. If an image is present, the settings form should have a checkbox to choose whether or not to lazy load the image.
